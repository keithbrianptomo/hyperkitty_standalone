========================================
HyperKitty - Archiver for GNU Mailman v3
========================================

**This component is obsolete.  If you have problems with an existing
installation, please file the report against the "example_project" component of
the HyperKitty project: https://gitlab.com/mailman/hyperkitty/issues/.

HyperKitty is an open source Django application under development. It aims at
providing a web interface to access GNU Mailman archives.

This is the hyperkitty_standalone component, which provides the Django project
files and some examples to configure Apache and Mailman.

All documentation on installing HyperKitty can be found in the documentation
provided by the ``HyperKitty`` python package. It is also available online at
the following URL: http://hyperkitty.readthedocs.org.


License
=======

HyperKitty is licensed under the `GPL v3.0`_

.. _GPL v3.0: http://www.gnu.org/licenses/gpl-3.0.html
